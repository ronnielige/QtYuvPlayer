#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

extern "C"{
#include <SDL.h>
}

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_toolButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;


    SDL_Window*   m_sdl_wd;
    SDL_Renderer* m_sdl_rnd;
    SDL_Texture*  m_sdl_txture;
    SDL_Rect      m_disp_rect;
};
#endif // MAINWINDOW_H
