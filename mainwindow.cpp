#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QWidget>
#include <malloc.h>
#include <QTime>
#include <stdio.h>
#include <QThread>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //setWindowFlag(Qt::FramelessWindowHint);

    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO))
    {
        QMessageBox::warning(NULL, "warn", QString("Could not initialize SDL - %s\n").arg(SDL_GetError()));
    }

    m_sdl_wd     = SDL_CreateWindowFrom((void *)ui->display_widget->winId());
    m_sdl_rnd    = SDL_CreateRenderer(m_sdl_wd, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    m_sdl_txture = SDL_CreateTexture(m_sdl_rnd, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, 1920, 1080);
    m_disp_rect.x = 0;
    m_disp_rect.y = 0;
    m_disp_rect.w = 1920;
    m_disp_rect.h = 1080;
}

MainWindow::~MainWindow()
{
    SDL_Quit();
    delete ui;
}


void MainWindow::on_toolButton_clicked()
{
    QMessageBox::warning(NULL, "tips", QString("Hello World Pressed"));
}

void MainWindow::on_pushButton_3_clicked()
{
    //this->showMinimized();
    close();
}

void MainWindow::on_pushButton_clicked()
{
    QTime time;
    int frm_size = 1920 * 1080 * 3 / 2;
    static int frm = 0;

    //this->showFullScreen();
    for(frm = 0; frm < 300; frm++)
    {
        unsigned char* pBuffer = (unsigned char*)malloc(1920 * 1080 * 3);
        FILE* f = fopen("F:\\HEVC_YUV_SEQ\\Bbasketballdrive_1920x1080_8_50_500.yuv", "rb");
        fseek(f, frm * frm_size, SEEK_SET);
        fread(pBuffer, 1, frm_size, f);
        fclose(f);

        SDL_UpdateYUVTexture(m_sdl_txture, NULL, pBuffer, 1920, pBuffer + 1920 * 1080, 960, pBuffer + 1920 * 1080 + 960 * 540, 960);
        SDL_RenderCopyEx(m_sdl_rnd, m_sdl_txture, NULL, NULL, 0, NULL, SDL_FLIP_NONE);
        SDL_RenderPresent(m_sdl_rnd);
        free(pBuffer);

        QThread::msleep(10);
    }
}
